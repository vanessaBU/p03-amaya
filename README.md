Apple Jump
-----------

The objective of this game is to help the hungry jumper catch the delicious apple. Use the slider to jump left and right. If you reach either end, simply let go and the slider will return to its original center position. If you keep holding it down, you may send the jumper flying out of control, so keep this in mind. You can safely land on the clouds but be careful not to fall into the abyss, or then it's game over. If you spend too much time still, new clouds will generate and older clouds will be pushed down. This can be good as it gives you more potential clouds to jump on, but it can also mess up your jumps and you may fall down. Sometimes you can get lucky and the apple will come to you.

Good luck catching the apple!