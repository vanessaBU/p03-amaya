//
//  ViewController.m
//  p03-amaya
//
//  Created by Vanessa Amaya on 2/12/17.
//  Copyright © 2017 Vanessa Amaya. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated {
    
    //developer.apple.com/library/content/documentation/GraphicsAnimation/Conceptual/SpriteKit_PG/GettingStarted/GettingStarted.html
    StartScene *startScene = [[StartScene alloc] initWithSize:CGSizeMake(768, 1024)  ];
    SKView *spriteView = (SKView *)self.view;
    [spriteView presentScene:startScene];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
