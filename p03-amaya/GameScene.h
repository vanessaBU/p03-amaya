//
//  GameScene.h
//  p03-amaya
//
//  Created by Vanessa Amaya on 2/17/17.
//  Copyright © 2017 Vanessa Amaya. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <SpriteKit/SpriteKit.h>

#import "GameScene.h"

//stackoverflow.com/questions/22495285/sprite-kit-collision-detection
@interface GameScene : SKScene <SKPhysicsContactDelegate>

@property (nonatomic, strong) SKScene *parentScene;

@end
