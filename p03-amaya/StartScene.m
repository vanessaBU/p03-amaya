//
//  StartScene.m
//  p03-amaya
//
//  Created by Vanessa Amaya on 2/17/17.
//  Copyright © 2017 Vanessa Amaya. All rights reserved.
//

#import "StartScene.h"

@interface StartScene ()

@end

@implementation StartScene

- (void)didMoveToView:(SKView *)view {
    
    [self LoadSceneContents];
}

- (void)LoadSceneContents {
    
    self.backgroundColor = [SKColor colorWithRed:135./255. green:206./255. blue:250./255. alpha:1.0f];
    self.scaleMode = SKSceneScaleModeAspectFit;
    [self addChild: [self newStartNode]];
}

- (SKLabelNode *)newStartNode {
    
    SKLabelNode *startNode = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    startNode.text = @"Start Apple Jump";
    startNode.fontSize = 38;
    startNode.position = CGPointMake(CGRectGetMidX(self.frame),CGRectGetMidY(self.frame));
    startNode.name = @"startGame";
    return startNode;
}

- (void)touchesBegan:(NSSet *)touches
           withEvent:(UIEvent *)event {
    
    SKNode *startNode = [self childNodeWithName:@"startGame"];
    if (startNode != nil) {
        startNode.name = nil;
        SKAction *fadeAway = [SKAction fadeOutWithDuration: 1.];
        GameScene *gameScene  = [[GameScene alloc] initWithSize:self.size];
        [startNode runAction:fadeAway];
        gameScene.parentScene = self;
        [self.view presentScene:gameScene];
    }
}

@end
