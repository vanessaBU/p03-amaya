//
//  ViewController.h
//  p03-amaya
//
//  Created by Vanessa Amaya on 2/12/17.
//  Copyright © 2017 Vanessa Amaya. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <UIKit/UIKit.h>

#import "StartScene.h"

@interface ViewController : UIViewController

@end
