//
//  GameScene.m
//  p03-amaya
//
//  Created by Vanessa Amaya on 2/17/17.
//  Copyright © 2017 Vanessa Amaya. All rights reserved.
//

#import "GameScene.h"

int const GRID_WIDTH = 11;
int const GRID_HEIGHT = 15;
float const BOUNCY = 0.;
float const DAMPY = 1.;
float const ANG_DAMP = 1.;
float const FRICTION = 1.;
static int CLOUD_SIZE;

float prevSlider;
bool appleExists;
bool gameWon;

bool jumpingStarted;
CFTimeInterval lastJump;
CFTimeInterval lastApple;

static const int jumperHit = 1;
static const int appleHit = 2;

@interface GameScene ()

@property BOOL contentCreated;
@property (nonatomic, strong) SKSpriteNode *jumper;
@property (nonatomic, strong) UISlider *slider;

@end

@implementation GameScene

- (id)initWithSize:(CGSize)size
        initParent:(SKScene *)parent {
    
    self.parentScene = parent;
    jumpingStarted = false;
    gameWon = false;
    return self;
}

- (void)didMoveToView:(SKView *)view {
    
    if (!self.contentCreated) {
        [self createSceneContents];
        self.contentCreated = YES;
    }
    jumpingStarted = false;
    gameWon = false;
    [self addSlider];
    self.physicsWorld.contactDelegate = self;
}

- (void)createSceneContents {
    
    self.backgroundColor = [SKColor colorWithRed:135./255. green:206./255. blue:250./255. alpha:1.0f];
    [self addClouds];
    [self addJumper];
    self.physicsWorld.gravity = CGVectorMake(0, -2.);
    appleExists = false;
}

- (void)addSlider {
    
    CGFloat frameWidth = self.view.bounds.size.width - CLOUD_SIZE/4;
    CGRect frame = CGRectMake(0., 0., frameWidth, 5.);
    _slider = [[UISlider alloc] initWithFrame:frame];
    [_slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
    [_slider setBackgroundColor:[UIColor clearColor]];
    _slider.minimumValue = 0.;
    _slider.maximumValue = 1.;
    _slider.continuous = YES;
    _slider.value = 0.5;
    _slider.center = CGPointMake(self.view.center.x, self.view.bounds.size.height - CLOUD_SIZE/2);
    [self.view addSubview:_slider];
    prevSlider = _slider.value;
}

-(void)sliderAction:(id)sender
{
    //stackoverflow.com/questions/19286610/constant-movement-in-spritekit
    UISlider *slider = (UISlider*)sender;
    CGFloat sliderVal = slider.value;
    CGFloat sliderDiff = sliderVal - prevSlider;
    CGFloat dx, dy;
    if (fabsf(_jumper.physicsBody.velocity.dy) > 2.) {
        dx = 100.;
        dy = 0.;
    }
    else {
        dx = 200.;
        dy = 20.;
    }
    dx *= sliderDiff;
    [_jumper.physicsBody applyImpulse:CGVectorMake(dx, dy)];
    // check if slider has moved to either ends (should move back to center so that user can jump left or right)
    if (sliderVal == _slider.minimumValue || sliderVal == _slider.maximumValue) {
        _slider.value = 0.5;
    }
    // reset slider value for next jump (according to actual slider value)
    prevSlider = _slider.value;
    // store time jumped (used for calculating new cloud generation and old cloud drops)
    lastJump = CACurrentMediaTime();
    //
    if (!jumpingStarted) {
        jumpingStarted = true;
    }
}

- (void)addJumper {
    
    _jumper = [[SKSpriteNode alloc] initWithImageNamed:@"drool.png"];
    _jumper.position = CGPointMake(CLOUD_SIZE, CLOUD_SIZE * 2 + 1);
    _jumper.name = @"jumper";
    _jumper.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:_jumper.size.width/2];
    _jumper.physicsBody.usesPreciseCollisionDetection = YES;
    _jumper.physicsBody.restitution = BOUNCY;
    _jumper.physicsBody.linearDamping = DAMPY;
    _jumper.physicsBody.angularDamping = ANG_DAMP;
    _jumper.physicsBody.friction = FRICTION;
    _jumper.physicsBody.categoryBitMask = jumperHit;
    _jumper.physicsBody.contactTestBitMask = appleHit;
    _jumper.physicsBody.collisionBitMask = appleHit;
    [self addChild:_jumper];
}

- (void)addClouds {
    
    int gridCols = GRID_WIDTH;
    int gridRows = GRID_HEIGHT;
    int maxClouds = gridCols * gridRows * 0.2;
    int minClouds = maxClouds * 0.5;
    SKTexture *cloudTexture = [SKTexture textureWithImageNamed:@"cloud.png"];
    CGSize cloudSize = CGSizeMake(cloudTexture.size.width, cloudTexture.size.height/4);
    // starting cloud
    CLOUD_SIZE = cloudSize.width;
    SKSpriteNode *startCloud = [[SKSpriteNode alloc] initWithTexture:cloudTexture];
    startCloud.name = @"cloud";
    startCloud.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:cloudSize];
    startCloud.physicsBody.usesPreciseCollisionDetection = YES;
    startCloud.physicsBody.dynamic = NO;
    startCloud.physicsBody.restitution = BOUNCY;
    startCloud.physicsBody.linearDamping = DAMPY;
    startCloud.physicsBody.angularDamping = ANG_DAMP;
    startCloud.physicsBody.friction = FRICTION;
    startCloud.position = CGPointMake(cloudTexture.size.width, cloudTexture.size.height + CLOUD_SIZE);
    [self addChild:startCloud];
    maxClouds--;
    int minCol, minRow, maxRow;
    int nClouds;
    // lower half
    minCol = 2;
    minRow = 2;
    maxRow = gridRows / 2;
    int minLClouds = minClouds / 2;
    int maxLClouds = maxClouds / 2;
    nClouds = minLClouds + arc4random() % (maxLClouds - minLClouds + 1);
    for (int i = 0; i < nClouds; i++) {
        // random position
        int xRandom = (minCol + arc4random() % (gridCols - minCol + 1)) * cloudTexture.size.width;
        int yRandom = (minRow + arc4random() % (maxRow - minRow + 1)) * cloudTexture.size.height;
        // cloud
        SKSpriteNode *cloud = [[SKSpriteNode alloc] initWithTexture:cloudTexture];
        cloud.name = @"cloud";
        cloud.position = CGPointMake(xRandom, yRandom);
        cloud.physicsBody = [SKPhysicsBody bodyWithTexture:cloudTexture size:cloudSize];
        cloud.physicsBody.usesPreciseCollisionDetection = YES;
        cloud.physicsBody.dynamic = NO;
        cloud.physicsBody.restitution = BOUNCY;
        cloud.physicsBody.linearDamping = DAMPY;
        cloud.physicsBody.angularDamping = ANG_DAMP;
        cloud.physicsBody.friction = FRICTION;
        [self addChild:cloud];
    }
    maxClouds -= nClouds;
    // upper half
    minCol = 1;
    minRow = gridRows / 2 + 1;
    maxRow = gridRows;
    int minUClouds = minClouds - nClouds;
    nClouds = minUClouds + arc4random() % (maxClouds - minUClouds + 1);
    for (int i = 0; i < nClouds; i++) {
        // random position
        int xRandom = (minCol + arc4random() % (gridCols - minCol + 1)) * cloudTexture.size.width;
        int yRandom = (minRow + arc4random() % (maxRow - minRow + 1)) * cloudTexture.size.height;
        // cloud
        SKSpriteNode *cloud = [[SKSpriteNode alloc] initWithTexture:cloudTexture];
        cloud.name = @"cloud";
        cloud.position = CGPointMake(xRandom, yRandom);
        cloud.physicsBody = [SKPhysicsBody bodyWithTexture:cloudTexture size:cloudSize];
        cloud.physicsBody.usesPreciseCollisionDetection = YES;
        cloud.physicsBody.dynamic = NO;
        cloud.physicsBody.restitution = BOUNCY;
        cloud.physicsBody.linearDamping = DAMPY;
        cloud.physicsBody.angularDamping = ANG_DAMP;
        cloud.physicsBody.friction = FRICTION;
        [self addChild:cloud];
    }
}

- (void)addMoreClouds {
    
    SKTexture *cloudTexture = [SKTexture textureWithImageNamed:@"cloud.png"];
    CGSize cloudSize = CGSizeMake(cloudTexture.size.width, cloudTexture.size.height/4);
    int nClouds = 1 + arc4random() % 6;
    for (int i = 0; i < nClouds; i++) {
        // random position
        int xRandom = (1 + arc4random() % (GRID_WIDTH - 1 + 1)) * CLOUD_SIZE;
        int yRandom = ((GRID_HEIGHT - 3) + arc4random() % 2) * CLOUD_SIZE;
        // cloud
        SKSpriteNode *cloud = [[SKSpriteNode alloc] initWithTexture:cloudTexture];
        cloud.name = @"cloud";
        cloud.position = CGPointMake(xRandom, yRandom);
        cloud.physicsBody = [SKPhysicsBody bodyWithTexture:cloudTexture size:cloudSize];
        cloud.physicsBody.usesPreciseCollisionDetection = YES;
        cloud.physicsBody.dynamic = NO;
        cloud.physicsBody.restitution = BOUNCY;
        cloud.physicsBody.linearDamping = DAMPY;
        cloud.physicsBody.angularDamping = ANG_DAMP;
        cloud.physicsBody.friction = FRICTION;
        [self addChild:cloud];
    }
}

-(void)addSporadicApple {
    
    if (!appleExists) {
        
        appleExists = true;
        SKTexture *texture = [SKTexture textureWithImageNamed:@"apple.png"];
        int xRandom = (1 + arc4random() % GRID_WIDTH) * texture.size.width;
        int yRandom = (1 + arc4random() % GRID_HEIGHT) * texture.size.height;
        if (fabsf(_jumper.position.x - xRandom) <= 10. && fabsf(_jumper.position.y - yRandom) <= 10.) {
            appleExists = false;
            return;
        }
        SKSpriteNode *apple = [[SKSpriteNode alloc] initWithTexture:texture];
        apple.name = @"apple";
        apple.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:texture.size];
        apple.physicsBody.usesPreciseCollisionDetection = YES;
        apple.physicsBody.dynamic = NO;
        apple.physicsBody.restitution = BOUNCY;
        apple.physicsBody.linearDamping = DAMPY;
        apple.physicsBody.angularDamping = ANG_DAMP;
        apple.physicsBody.friction = FRICTION;
        apple.position = CGPointMake(xRandom, yRandom);
        apple.physicsBody.categoryBitMask = appleHit;
        apple.physicsBody.contactTestBitMask = jumperHit;
        apple.physicsBody.collisionBitMask = jumperHit;
        [self addChild:apple];
        lastApple = CACurrentMediaTime();
    }
}

-(void)didSimulatePhysics {
    
    if (gameWon) {
        return;
    }
    
    if (fabsf(_jumper.physicsBody.velocity.dy) < 2.) {
        _jumper.physicsBody.velocity = CGVectorMake(0., 0.);
    }
    if (_jumper.position.y < 0) {
        [_jumper removeFromParent];
        [self enumerateChildNodesWithName:@"cloud" usingBlock:^(SKNode *node, BOOL *stop) {
            [node removeFromParent];
        }];
        [_slider removeFromSuperview];
        [self.view presentScene:_parentScene];
    }
    else {
        [self enumerateChildNodesWithName:@"cloud" usingBlock:^(SKNode *node, BOOL *stop) {
            if (node.position.y < 0)
                [node removeFromParent];
        }];
        CFTimeInterval elapsedTime = CACurrentMediaTime() - lastJump;
        int moveDownBy = CLOUD_SIZE * 4 * -1;
        if (jumpingStarted && elapsedTime >= 1. && _jumper.physicsBody.velocity.dy == 0. && _jumper.physicsBody.velocity.dx == 0.) {
            [self enumerateChildNodesWithName:@"cloud" usingBlock:^(SKNode *node, BOOL *stop) {
                SKAction *moveCloud = [SKAction sequence:@[[SKAction waitForDuration:1.],
                                                           [SKAction moveByX:0. y:moveDownBy duration:1.]]];
                [node runAction:moveCloud];
            }];
            [self addMoreClouds];
            lastJump = CACurrentMediaTime();
        }
    }
    if (!appleExists) {
        [self addSporadicApple];
    }
    else {
        CFTimeInterval elapsedApple = CACurrentMediaTime() - lastApple;
        if (elapsedApple >= 0.25) {
            [self enumerateChildNodesWithName:@"apple" usingBlock:^(SKNode *node, BOOL *stop) {
                [node removeFromParent];
            }];
            appleExists = false;
        }
    }
}

- (void)didBeginContact:(SKPhysicsContact *)contact {
    
    if (contact.bodyA.categoryBitMask == appleHit || contact.bodyB.categoryBitMask == appleHit) {
        gameWon = true;
        [_jumper removeFromParent];
        [self enumerateChildNodesWithName:@"apple" usingBlock:^(SKNode *node, BOOL *stop) {
            [node removeFromParent];
        }];
        [self enumerateChildNodesWithName:@"cloud" usingBlock:^(SKNode *node, BOOL *stop) {
            [node removeFromParent];
        }];
        [_slider removeFromSuperview];
        [self addChild: [self winnerNode]];
    }
}

- (SKLabelNode *)winnerNode {
    
    SKLabelNode *winNode = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    winNode.text = @"You Win!";
    winNode.fontSize = 38;
    winNode.position = CGPointMake(CGRectGetMidX(self.frame),CGRectGetMidY(self.frame));
    winNode.name = @"winGame";
    return winNode;
}

- (void)touchesBegan:(NSSet *)touches
           withEvent:(UIEvent *)event {
    
    SKNode *winNode = [self childNodeWithName:@"winGame"];
    if (winNode != nil) {
        winNode.name = nil;
        SKAction *fadeAway = [SKAction fadeOutWithDuration: 1.];
        [winNode runAction:fadeAway];
        [self.view presentScene:_parentScene];
    }
}

@end
