//
//  StartScene.h
//  p03-amaya
//
//  Created by Vanessa Amaya on 2/17/17.
//  Copyright © 2017 Vanessa Amaya. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

#import "GameScene.h"

@interface StartScene : SKScene

@end
