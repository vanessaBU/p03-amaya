//
//  main.m
//  p03-amaya
//
//  Created by Vanessa Amaya on 2/12/17.
//  Copyright © 2017 Vanessa Amaya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
